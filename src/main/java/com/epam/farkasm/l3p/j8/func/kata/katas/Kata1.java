package com.epam.farkasm.l3p.j8.func.kata.katas;

import com.epam.farkasm.l3p.j8.func.kata.model.Movie;

import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

import static com.epam.farkasm.l3p.j8.func.kata.util.KataHelper.mapOf;
import static java.util.stream.Collectors.toList;

/*
  Goal: use map() to project an array of videos into an array of {id, title}-pairs
  DataSource: DataUtil.getMovies()
  Output: List of ImmutableMap.of("id", "5", "title", "Bad Boys")
*/
public class Kata1 {

  public static List<Map<String, Object>> videosToMap(Stream<Movie> movies) {
    return movies
      .map(
        movie -> mapOf(
          "id", movie.getId(),
          "title", movie.getTitle()
        ))
      .collect(toList());
  }
}
