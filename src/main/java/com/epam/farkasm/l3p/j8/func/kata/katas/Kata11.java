package com.epam.farkasm.l3p.j8.func.kata.katas;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.epam.farkasm.l3p.j8.func.kata.util.DataUtil;

import java.util.List;
import java.util.Map;

/*
  Goal: Create a data structure from the given data:

  This time we have 4 separate arrays each containing lists, videos, box arts, and bookmarks
  respectively.
  Each object has a parent id, indicating its parent.
  We want to build an array of list objects, each with a name and a videos array.
  The videos array will contain the video's id, title, bookmark time, and smallest box art url.
  In other words we want to build the following structure:

  [
    {
      "name": "New Releases",
      "videos": [
        {
          "id": 65432445,
          "title": "The Chamber",
          "time": 32432,
          "boxArt": "http://cdn-0.nflximg.com/images/2891/TheChamber130.jpg"
        },
        {
          "id": 675465,
          "title": "Fracture",
          "time": 3534543,
          "boxArt": "http://cdn-0.nflximg.com/images/2891/Fracture120.jpg"
        }
      ]
    },
    {
      "name": "Thrillers",
      "videos": [
        {
          "id": 70111470,
          "title": "Die Hard",
          "time": 645243,
          "boxArt": "http://cdn-0.nflximg.com/images/2891/DieHard150.jpg"
        },
        {
          "id": 654356453,
          "title": "Bad Boys",
          "time": 984934,
          "boxArt": "http://cdn-0.nflximg.com/images/2891/BadBoys140.jpg"
        }
      ]
    }
  ]

  DataSource: DataUtil.getLists(), DataUtil.getVideos(), DataUtil.getBoxArts(),
              DataUtil.getBookmarkList()
  Output: the given data structure
*/
public class Kata11 {

  public static List<Map> execute() {
    List<Map> lists = DataUtil.getLists();
    List<Map> videos = DataUtil.getVideos();
    List<Map> boxArts = DataUtil.getBoxArts();
    List<Map> bookmarkList = DataUtil.getBookmarkList();
    return ImmutableList.of(
      ImmutableMap.of(
        "name", "someName",
        "videos", ImmutableList.of(
          ImmutableMap.of(
            "id", 5,
            "title", "The Chamber",
            "time", 123,
            "boxart", "someUrl"
          )
        )
      )
    );
  }
}
