package com.epam.farkasm.l3p.j8.func.kata.katas;

import com.epam.farkasm.l3p.j8.func.kata.model.Movie;

import java.util.List;

import static com.epam.farkasm.l3p.j8.func.kata.util.DataUtil.getMovies;
import static java.util.stream.Collectors.toList;

/*
  Goal: Chain filter() and map() to collect the ids of videos that have a rating of 5.0
  DataSource: DataUtil.getMovies()
  Output: List of Integers
*/
public class Kata2 {

  public static List<Integer> execute() {
    return getMovies().stream()
      .filter(movie -> movie.getRating() == 5.0)
      .map(Movie::getId)
      .collect(toList());
  }
}
