package com.epam.farkasm.l3p.j8.func.kata.katas;

import com.epam.farkasm.l3p.j8.func.kata.model.Movie;

import java.util.List;

import static com.epam.farkasm.l3p.j8.func.kata.util.DataUtil.getMovieLists;
import static java.util.stream.Collectors.toList;

/*
 Goal: Use map() and flatMap() to project and flatten the movieLists into an array of video ids
       (flatMap(c -> c.stream()))
 DataSource: DataUtil.getMovieLists()
 Output: List of Integers
*/
public class Kata3 {

  public static List<Integer> execute() {
    return getMovieLists().stream()
      .flatMap(movieList -> movieList.getVideos().stream())
      .map(Movie::getId)
      .collect(toList());
  }
}
