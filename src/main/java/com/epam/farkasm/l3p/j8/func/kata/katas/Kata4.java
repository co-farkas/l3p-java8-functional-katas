package com.epam.farkasm.l3p.j8.func.kata.katas;

import com.google.common.collect.ImmutableMap;

import java.util.List;
import java.util.Map;

import static com.epam.farkasm.l3p.j8.func.kata.util.DataUtil.getMovieLists;
import static java.util.stream.Collectors.toList;

/*
  Goal: Retrieve id, title, and a 150x200 box art url for every video
  DataSource: DataUtil.getMovieLists()
  Output: List of ImmutableMap.of("id", "5", "title", "Bad Boys", "boxArt": BoxArt)
*/
public class Kata4 {

  @SuppressWarnings("ConstantConditions")
  public static List<Map<String, Object>> execute() {
    return getMovieLists().stream()
      .flatMap(movieList -> movieList.getVideos().stream())
      .map(movie ->
             ImmutableMap.of(
               "id", String.valueOf(movie.getId()),
               "title", movie.getTitle(),
               "boxArt", movie.getBoxArts()
                 .stream()
                 .filter(boxArt -> boxArt.getWidth() == 150)
                 .filter(boxArt -> boxArt.getHeight() == 200)
                 .findFirst().get()
             )
      )
      .collect(toList());
  }
}
