package com.epam.farkasm.l3p.j8.func.kata.katas;

import com.epam.farkasm.l3p.j8.func.kata.model.Movie;

import static com.epam.farkasm.l3p.j8.func.kata.util.DataUtil.getMovies;

/*
  Goal: Retrieve the largest rating using reduce()
  DataSource: DataUtil.getMovies()
  Output: Double
*/
public class Kata5 {

  public static Double execute() {
    return getMovies().stream()
      .map(Movie::getRating)
      .reduce((a, b) -> a < b ? b : a)
      .orElse(null);
  }
}
