package com.epam.farkasm.l3p.j8.func.kata.katas;

import com.epam.farkasm.l3p.j8.func.kata.model.BoxArt;

import static com.epam.farkasm.l3p.j8.func.kata.util.DataUtil.getMovies;
import static com.epam.farkasm.l3p.j8.func.kata.util.KataHelper.sizeOf;

/*
  Goal: Retrieve the url of the largest boxArt using map() and reduce()
  DataSource: DataUtil.getMovieLists()
  Output: String
*/
public class Kata6 {

  public static String execute() {
    return getMovies().stream()
      .flatMap(movie -> movie.getBoxArts().stream())
      .reduce((a, b) -> sizeOf(a) < sizeOf(b) ? b : a)
      .orElseGet(BoxArt::new)
      .getUrl();
  }
}
