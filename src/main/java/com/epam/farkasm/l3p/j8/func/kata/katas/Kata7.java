package com.epam.farkasm.l3p.j8.func.kata.katas;

import com.epam.farkasm.l3p.j8.func.kata.model.BoxArt;
import com.google.common.collect.ImmutableMap;

import java.util.List;
import java.util.Map;

import static com.epam.farkasm.l3p.j8.func.kata.util.KataHelper.sizeOf;
import static com.epam.farkasm.l3p.j8.func.kata.util.DataUtil.getMovieLists;
import static java.util.stream.Collectors.toList;

/*
  Goal: Retrieve the id, title, and smallest box art url for every video
  DataSource: DataUtil.getMovieLists()
  Output: List of ImmutableMap.of("id", "5", "title", "Bad Boys", "boxArt": "url)
*/
public class Kata7 {

  public static List<Map> execute() {

    return getMovieLists().stream()
      .flatMap(movieList -> movieList.getVideos().stream())
      .map(movie ->
             ImmutableMap.of(
               "id", String.valueOf(movie.getId()),
               "title", movie.getTitle(),
               "boxArt", movie.getBoxArts().stream()
                 .reduce((a, b) -> sizeOf(a) < sizeOf(b) ? a : b)
                 .orElseGet(BoxArt::new)
                 .getUrl()
             )
      )
      .collect(toList());
  }
}
