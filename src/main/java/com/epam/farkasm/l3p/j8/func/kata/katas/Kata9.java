package com.epam.farkasm.l3p.j8.func.kata.katas;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.epam.farkasm.l3p.j8.func.kata.model.MovieList;
import com.epam.farkasm.l3p.j8.func.kata.util.DataUtil;

import java.util.Date;
import java.util.List;
import java.util.Map;

/*
  Goal: Retrieve each video's id, title, middle interesting moment time, and smallest box art url
  DataSource: DataUtil.getMovies()
  Output: List of ImmutableMap.of("id", 5, "title", "some title", "time", new Date(), "url", "someUrl")
*/
public class Kata9 {

  public static List<Map> execute() {
    List<MovieList> movieLists = DataUtil.getMovieLists();
    return ImmutableList.of(
      ImmutableMap.of("id", 5, "title", "some title", "time", new Date(), "url", "someUrl")
    );
  }
}
