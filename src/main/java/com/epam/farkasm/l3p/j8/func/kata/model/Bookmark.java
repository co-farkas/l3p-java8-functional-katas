package com.epam.farkasm.l3p.j8.func.kata.model;

import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;

import java.util.Date;

public class Bookmark {

  private Integer id;

  private Date time;

  public Bookmark() {
    // create with null values
  }

  public Bookmark(Integer id, Date time) {
    this.id = id;
    this.time = time;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public Date getTime() {
    return time;
  }

  public void setTime(Date time) {
    this.time = time;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    Bookmark bookmark = (Bookmark) o;
    return Objects.equal(id, bookmark.id) &&
      Objects.equal(time, bookmark.time);
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(id, time);
  }

  @Override
  public String toString() {
    return MoreObjects.toStringHelper(this)
      .add("id", id)
      .add("time", time)
      .toString();
  }
}
