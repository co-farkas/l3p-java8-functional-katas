package com.epam.farkasm.l3p.j8.func.kata.model;

import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;

public class BoxArt {

  private Integer width;

  private Integer height;

  private String url;

  public BoxArt() {
    // create with null values
  }

  public BoxArt(Integer width, Integer height, String uri) {
    this.width = width;
    this.height = height;
    this.url = uri;
  }

  public Integer getWidth() {
    return width;
  }

  public void setWidth(Integer width) {
    this.width = width;
  }

  public Integer getHeight() {
    return height;
  }

  public void setHeight(Integer height) {
    this.height = height;
  }

  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    BoxArt boxArt = (BoxArt) o;
    return Objects.equal(width, boxArt.width) &&
      Objects.equal(height, boxArt.height) &&
      Objects.equal(url, boxArt.url);
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(width, height, url);
  }

  @Override
  public String toString() {
    return MoreObjects.toStringHelper(this)
      .add("width", width)
      .add("height", height)
      .add("url", url)
      .toString();
  }
}
