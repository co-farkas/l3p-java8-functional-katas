package com.epam.farkasm.l3p.j8.func.kata.model;


import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;

import java.util.Date;

public class InterestingMoment {

  private String type;

  private Date time;

  public InterestingMoment() {
    // create with null values
  }

  public InterestingMoment(String type, Date time) {
    this.type = type;
    this.time = time;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public Date getTime() {
    return time;
  }

  public void setTime(Date time) {
    this.time = time;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    InterestingMoment that = (InterestingMoment) o;
    return Objects.equal(type, that.type) &&
      Objects.equal(time, that.time);
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(type, time);
  }

  @Override
  public String toString() {
    return MoreObjects.toStringHelper(this)
      .add("type", type)
      .add("time", time)
      .toString();
  }
}
