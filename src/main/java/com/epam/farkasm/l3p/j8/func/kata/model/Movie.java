package com.epam.farkasm.l3p.j8.func.kata.model;

import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;

import java.util.List;

public class Movie {

  private Integer id;

  private String title;

  private List<BoxArt> boxArts;

  private String uri;

  private Double rating;

  private List<Bookmark> bookmark;

  private List<InterestingMoment> interestingMoments;

  public Movie() {
    // create with null values
  }

  public Movie(
    Integer id,
    String title,
    List<BoxArt> boxArts,
    String uri,
    Double rating,
    List<Bookmark> bookmark,
    List<InterestingMoment> interestingMoments
  ) {
    this.id = id;
    this.title = title;
    this.boxArts = boxArts;
    this.uri = uri;
    this.rating = rating;
    this.bookmark = bookmark;
    this.interestingMoments = interestingMoments;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public List<BoxArt> getBoxArts() {
    return boxArts;
  }

  public void setBoxArts(List<BoxArt> boxArts) {
    this.boxArts = boxArts;
  }

  public String getUri() {
    return uri;
  }

  public void setUri(String uri) {
    this.uri = uri;
  }

  public Double getRating() {
    return rating;
  }

  public void setRating(Double rating) {
    this.rating = rating;
  }

  public List<Bookmark> getBookmark() {
    return bookmark;
  }

  public void setBookmark(List<Bookmark> bookmark) {
    this.bookmark = bookmark;
  }

  public List<InterestingMoment> getInterestingMoments() {
    return interestingMoments;
  }

  public void setInterestingMoments(List<InterestingMoment> interestingMoments) {
    this.interestingMoments = interestingMoments;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) { return true; }
    if (o == null || getClass() != o.getClass()) { return false; }
    Movie movie = (Movie) o;
    return Objects.equal(id, movie.id) &&
      Objects.equal(title, movie.title) &&
      Objects.equal(boxArts, movie.boxArts) &&
      Objects.equal(uri, movie.uri) &&
      Objects.equal(rating, movie.rating) &&
      Objects.equal(bookmark, movie.bookmark) &&
      Objects.equal(interestingMoments, movie.interestingMoments);
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(id, title, boxArts, uri, rating, bookmark, interestingMoments);
  }

  @Override
  public String toString() {
    return MoreObjects.toStringHelper(this)
      .add("id", id)
      .add("title", title)
      .add("boxArts", boxArts)
      .add("uri", uri)
      .add("rating", rating)
      .add("bookmark", bookmark)
      .add("interestingMoments", interestingMoments)
      .toString();
  }
}
