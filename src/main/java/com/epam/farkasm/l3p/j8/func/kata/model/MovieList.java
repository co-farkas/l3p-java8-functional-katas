package com.epam.farkasm.l3p.j8.func.kata.model;

import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;

import java.util.List;

public class MovieList {

  private String name;

  private List<Movie> videos;

  public MovieList() {
    // create with null values
  }

  public MovieList(String name, List<Movie> videos) {
    this.name = name;
    this.videos = videos;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public List<Movie> getVideos() {
    return videos;
  }

  public void setVideos(List<Movie> videos) {
    this.videos = videos;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    MovieList movieList = (MovieList) o;
    return Objects.equal(name, movieList.name) &&
      Objects.equal(videos, movieList.videos);
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(name, videos);
  }

  @Override
  public String toString() {
    return MoreObjects.toStringHelper(this)
      .add("name", name)
      .add("videos", videos)
      .toString();
  }
}
