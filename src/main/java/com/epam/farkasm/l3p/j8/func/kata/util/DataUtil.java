package com.epam.farkasm.l3p.j8.func.kata.util;

import com.epam.farkasm.l3p.j8.func.kata.model.*;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class DataUtil {

  public static List<Movie> getMovies() {
    List<Movie> result = new ArrayList<>();
    result.add(new Movie(70111470,
      "Die Hard",
      ImmutableList.of(
        new BoxArt(150, 200, "http://cdn-0.nflximg.com/images/2891/DieHard150.jpg"),
        new BoxArt(200, 200, "http://cdn-0.nflximg.com/images/2891/DieHard200.jpg")
      ),
      "http://api.netflix.com/catalog/titles/movies/70111470",
      4.0,
      new ArrayList<>(),
      ImmutableList.of(
        new InterestingMoment("End", new Date()),
        new InterestingMoment("Middle", new Date()),
        new InterestingMoment("Start", new Date())
      )));
    result.add(new Movie(654356453,
      "Bad Boys",
      ImmutableList.of(
        new BoxArt(200, 200, "http://cdn-0.nflximg.com/images/2891/BadBoys200.jpg"),
        new BoxArt(150, 200, "http://cdn-0.nflximg.com/images/2891/BadBoys150.jpg")
      ),
      "http://api.netflix.com/catalog/titles/movies/70111470",
      5.0,
      ImmutableList.of(new Bookmark(432534, new Date())),
      ImmutableList.of(
        new InterestingMoment("End", new Date()),
        new InterestingMoment("Middle", new Date()),
        new InterestingMoment("Start", new Date())
      )));
    result.add(new Movie(65432445,
      "The Chamber",
      ImmutableList.of(
        new BoxArt(150, 200, "http://cdn-0.nflximg.com/images/2891/TheChamber150.jpg"),
        new BoxArt(200, 200, "http://cdn-0.nflximg.com/images/2891/TheChamber200.jpg")
      ),
      "http://api.netflix.com/catalog/titles/movies/70111470",
      4.0,
      new ArrayList<>(),
      ImmutableList.of(
        new InterestingMoment("End", new Date()),
        new InterestingMoment("Middle", new Date()),
        new InterestingMoment("Start", new Date())
      )));
    result.add(new Movie(675465,
      "Fracture",
      ImmutableList.of(
        new BoxArt(200, 200, "http://cdn-0.nflximg.com/images/2891/Fracture200.jpg"),
        new BoxArt(150, 200, "http://cdn-0.nflximg.com/images/2891/Fracture150.jpg"),
        new BoxArt(300, 200, "http://cdn-0.nflximg.com/images/2891/Fracture300.jpg")
      ),
      "http://api.netflix.com/catalog/titles/movies/70111470",
      5.0,
      ImmutableList.of(new Bookmark(432534, new Date())),
      ImmutableList.of(
        new InterestingMoment("End", new Date()),
        new InterestingMoment("Middle", new Date()),
        new InterestingMoment("Start", new Date())
      )
    ));
    return result;
  }

  public static List<MovieList> getMovieLists() {
    List<MovieList> result = new ArrayList<>();
    result.add(new MovieList("New Releases", getMovies().subList(0, 2)));
    result.add(new MovieList("Dramas", getMovies().subList(2, 4)));
    return result;
  }

  public static List<Bookmark> getBookMarks() {
    List<Bookmark> result = new ArrayList<>();
    result.add(new Bookmark(470, new Date()));
    result.add(new Bookmark(453, new Date()));
    result.add(new Bookmark(445, new Date()));
    return result;
  }

  public static List<Map> getLists() {
    List<Map> result = new ArrayList<>();
    result.add(ImmutableMap.of("id", 5434364, "name", "New Releases"));
    result.add(ImmutableMap.of("id", 65456475, "name", "Thrillers"));
    return result;
  }

  public static List<Map> getVideos() {
    List<Map> result = new ArrayList<>();
    result.add(ImmutableMap.of("listId", 5434364, "id", 65432445, "title", "The Chamber"));
    result.add(ImmutableMap.of("listId", 5434364, "id", 675465, "title", "Fracture"));
    result.add(ImmutableMap.of("listId", 65456475, "id", 70111470, "title", "Die Hard"));
    result.add(ImmutableMap.of("listId", 65456475, "id", 654356453, "title", "Bad Boys"));
    return result;
  }

  public static List<Map> getBoxArts() {
    List<Map> result = new ArrayList<>();
    result.add(ImmutableMap.of("videoId", 65432445, "width", 130, "height", 200, "url", "http://cdn-0.nflximg.com/images/2891/TheChamber130.jpg"));
    result.add(ImmutableMap.of("videoId", 65432445, "width", 200, "height", 200, "url", "http://cdn-0.nflximg.com/images/2891/TheChamber200.jpg"));
    result.add(ImmutableMap.of("videoId", 675465, "width", 200, "height", 200, "url", "http://cdn-0.nflximg.com/images/2891/Fracture200.jpg"));
    result.add(ImmutableMap.of("videoId", 675465, "width", 120, "height", 200, "url", "http://cdn-0.nflximg.com/images/2891/Fracture120.jpg"));
    result.add(ImmutableMap.of("videoId", 675465, "width", 300, "height", 200, "url", "http://cdn-0.nflximg.com/images/2891/Fracture300.jpg"));
    result.add(ImmutableMap.of("videoId", 70111470, "width", 150, "height", 200, "url", "http://cdn-0.nflximg.com/images/2891/DieHard150.jpg"));
    result.add(ImmutableMap.of("videoId", 70111470, "width", 200, "height", 200, "url", "http://cdn-0.nflximg.com/images/2891/DieHard200.jpg"));
    result.add(ImmutableMap.of("videoId", 654356453, "width", 200, "height", 200, "url", "http://cdn-0.nflximg.com/images/2891/BadBoys200.jpg"));
    result.add(ImmutableMap.of("videoId", 654356453, "width", 140, "height", 200, "url", "http://cdn-0.nflximg.com/images/2891/BadBoys140.jpg"));
    return result;
  }

  public static List<Map> getBookmarkList() {
    List<Map> result = new ArrayList<>();
    result.add(ImmutableMap.of("videoId", 65432445, "time", 32432));
    result.add(ImmutableMap.of("videoId", 675465, "time", 3534543));
    result.add(ImmutableMap.of("videoId", 70111470, "time", 645243));
    result.add(ImmutableMap.of("videoId", 654356453, "time", 984934));
    return result;
  }
}
