package com.epam.farkasm.l3p.j8.func.kata.util;

import com.epam.farkasm.l3p.j8.func.kata.model.BoxArt;
import com.google.common.collect.ImmutableMap;

public final class KataHelper {

  private KataHelper() {
    // prevent initialization
  }

  public static ImmutableMap<String, Object> mapOf(String k1, Integer v1, String k2, String v2) {
    return ImmutableMap.of(k1, v1, k2, v2);
  }

  public static long sizeOf(BoxArt boxArt) {
    return boxArt.getWidth().longValue() * boxArt.getHeight();
  }
}


