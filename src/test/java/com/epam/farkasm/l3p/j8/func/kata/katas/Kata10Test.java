package com.epam.farkasm.l3p.j8.func.kata.katas;

import com.epam.farkasm.l3p.j8.func.kata.katas.Kata10;
import org.junit.Ignore;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

public class Kata10Test {

  @Test
  @Ignore
  public void testExecute() {
    assertThat(Kata10.execute().size(), equalTo(2));
  }
}
