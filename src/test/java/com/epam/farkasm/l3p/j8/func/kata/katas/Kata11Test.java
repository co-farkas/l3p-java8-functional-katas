package com.epam.farkasm.l3p.j8.func.kata.katas;

import com.epam.farkasm.l3p.j8.func.kata.katas.Kata11;
import org.junit.Ignore;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

public class Kata11Test {

  @Test
  @Ignore
  public void testExecute() {
    assertThat(Kata11.execute().size(), equalTo(2));
  }
}
