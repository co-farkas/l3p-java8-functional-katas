package com.epam.farkasm.l3p.j8.func.kata.katas;

import com.google.common.collect.ImmutableList;
import org.junit.Test;

import static com.epam.farkasm.l3p.j8.func.kata.util.DataUtil.getMovies;
import static com.epam.farkasm.l3p.j8.func.kata.util.KataHelper.mapOf;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

public class Kata1Test {

  @Test
  public void testExecute() {
    assertThat(Kata1.videosToMap(getMovies().stream()), equalTo(
      ImmutableList.of(
        mapOf(
          "id", 70111470,
          "title", "Die Hard"
        ),
        mapOf(
          "id", 654356453,
          "title", "Bad Boys"
        ),
        mapOf(
          "id", 65432445,
          "title", "The Chamber"
        ),
        mapOf(
          "id", 675465,
          "title", "Fracture"
        )
      )
    ));
  }
}
