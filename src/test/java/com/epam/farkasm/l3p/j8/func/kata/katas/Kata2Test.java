package com.epam.farkasm.l3p.j8.func.kata.katas;

import com.google.common.collect.ImmutableList;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

public class Kata2Test {

  @Test
  public void testExecute() {
    assertThat(Kata2.execute(), equalTo(
      ImmutableList.of(
        654356453,
        675465
      )
    ));
  }
}
