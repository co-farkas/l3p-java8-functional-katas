package com.epam.farkasm.l3p.j8.func.kata.katas;

import com.google.common.collect.ImmutableList;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

public class Kata3Test {

  @Test
  public void testExecute() {
    assertThat(Kata3.execute(), equalTo(
      ImmutableList.of(
        70111470,
        654356453,
        65432445,
        675465
      )
    ));
  }
}
