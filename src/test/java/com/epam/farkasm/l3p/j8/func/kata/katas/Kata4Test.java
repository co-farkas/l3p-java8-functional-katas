package com.epam.farkasm.l3p.j8.func.kata.katas;

import com.epam.farkasm.l3p.j8.func.kata.model.BoxArt;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

public class Kata4Test {

  @Test
  public void testExecute() {
    assertThat(Kata4.execute(), equalTo(
      ImmutableList.of(
        ImmutableMap.of(
          "id", "70111470",
          "title", "Die Hard",
          "boxArt", new BoxArt(150, 200, "http://cdn-0.nflximg.com/images/2891/DieHard150.jpg")
        ),
        ImmutableMap.of(
          "id", "654356453",
          "title", "Bad Boys",
          "boxArt", new BoxArt(150, 200, "http://cdn-0.nflximg.com/images/2891/BadBoys150.jpg")
        ),
        ImmutableMap.of(
          "id", "65432445",
          "title", "The Chamber",
          "boxArt", new BoxArt(150, 200, "http://cdn-0.nflximg.com/images/2891/TheChamber150.jpg")
        ),
        ImmutableMap.of(
          "id", "675465",
          "title", "Fracture",
          "boxArt", new BoxArt(150, 200, "http://cdn-0.nflximg.com/images/2891/Fracture150.jpg")
        )
      )
    ));
  }
}
