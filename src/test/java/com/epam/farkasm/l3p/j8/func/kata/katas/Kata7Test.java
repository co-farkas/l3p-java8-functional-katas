package com.epam.farkasm.l3p.j8.func.kata.katas;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

public class Kata7Test {

  @Test
  public void testExecute() {
    assertThat(Kata7.execute(), equalTo(
      ImmutableList.of(
        ImmutableMap.of(
          "id", "70111470",
          "title", "Die Hard",
          "boxArt", "http://cdn-0.nflximg.com/images/2891/DieHard150.jpg"
        ),
        ImmutableMap.of(
          "id", "654356453",
          "title", "Bad Boys",
          "boxArt", "http://cdn-0.nflximg.com/images/2891/BadBoys150.jpg"
        ),
        ImmutableMap.of(
          "id", "65432445",
          "title", "The Chamber",
          "boxArt", "http://cdn-0.nflximg.com/images/2891/TheChamber150.jpg"
        ),
        ImmutableMap.of(
          "id", "675465",
          "title", "Fracture",
          "boxArt", "http://cdn-0.nflximg.com/images/2891/Fracture150.jpg"
        )
      )
    ));
  }
}
