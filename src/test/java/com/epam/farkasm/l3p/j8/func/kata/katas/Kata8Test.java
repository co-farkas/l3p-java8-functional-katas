package com.epam.farkasm.l3p.j8.func.kata.katas;

import org.junit.Ignore;
import org.junit.Test;

import static com.epam.farkasm.l3p.j8.func.kata.util.DataUtil.getMovies;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

public class Kata8Test {

  @Test
  @Ignore
  public void testExecute() {
    assertThat(Kata1.videosToMap(getMovies().stream()).size(), equalTo(3));
  }
}
