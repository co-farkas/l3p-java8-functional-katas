package com.epam.farkasm.l3p.j8.func.kata.katas;

import com.epam.farkasm.l3p.j8.func.kata.katas.Kata9;
import org.junit.Ignore;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

public class Kata9Test {

  @Test
  @Ignore
  public void testExecute() {
    assertThat(Kata9.execute().size(), equalTo(4));
  }
}
